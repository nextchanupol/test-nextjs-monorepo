import { FC } from 'react';

type Props = {
  originatingAppName: string;
  name: string;
  children?: never;
};
export const InfoCard: FC<Props> = (props) => {
  const { originatingAppName, name } = props;
  return (
    <div className="card">
      <img src="img_avatar.png" alt="Avatar" style={{ width: '100%' }} />
      <div className="container">
        <h4>
          <strong>{name}</strong>
        </h4>
        <p>{originatingAppName}</p>
      </div>
    </div>
  );
};
