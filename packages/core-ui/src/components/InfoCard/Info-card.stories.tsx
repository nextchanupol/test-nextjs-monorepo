import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import { InfoCard } from './info-card';

export default {
  title: 'Example/InfoCard',
  component: InfoCard,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as ComponentMeta<typeof InfoCard>;

const Template: ComponentStory<typeof InfoCard> = (args) => (
  <InfoCard originatingAppName="originatingAppName" name="name" />
);

export const Primary = Template.bind({});
Primary.args = {
  label: 'InfoCard',
};
